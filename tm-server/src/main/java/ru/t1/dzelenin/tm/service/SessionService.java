package ru.t1.dzelenin.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.api.repository.ISessionRepository;
import ru.t1.dzelenin.tm.api.service.ISessionService;
import ru.t1.dzelenin.tm.model.Session;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull ISessionRepository repository) {
        super(repository);
    }

    @Override
    public int getSize(@NotNull String userId) {
        return 0;
    }

}