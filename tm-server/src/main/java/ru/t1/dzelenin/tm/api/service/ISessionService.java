package ru.t1.dzelenin.tm.api.service;

import ru.t1.dzelenin.tm.model.Session;

public interface ISessionService extends IUserOwnerService<Session> {

}
