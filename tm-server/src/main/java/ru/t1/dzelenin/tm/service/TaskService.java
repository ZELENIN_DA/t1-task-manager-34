package ru.t1.dzelenin.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.api.repository.ITaskRepository;
import ru.t1.dzelenin.tm.api.service.ITaskService;
import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.enumerated.TaskSort;
import ru.t1.dzelenin.tm.exception.entity.StatusEmptyException;
import ru.t1.dzelenin.tm.exception.entity.TaskNotFoundException;
import ru.t1.dzelenin.tm.exception.field.*;
import ru.t1.dzelenin.tm.model.Task;

import java.util.Collections;
import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    @Override
    public Task create(@NotNull final String userId,
                       @NotNull final String name,
                       @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        add(userId, task);
        return task;
    }

    @Override
    public List<Task> findAll(@NotNull final String userId, @NotNull final TaskSort sort) {
        return null;
    }

    @Override
    public Task create(@NotNull final String userId, @NotNull final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        add(userId, task);
        return task;
    }

    @Override
    public Task updateById(@NotNull final String userId,
                           @NotNull final String id,
                           @NotNull final String name,
                           @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Task task = findOneById(userId, id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(@NotNull final String userId,
                              @NotNull final Integer index,
                              @NotNull final String name,
                              @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new IndexIncorrectException();
        @NotNull final Task task = findOneByIndex(userId, index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeTaskStatusIndex(@NotNull final String userId,
                                      @NotNull final Integer index,
                                      @NotNull final Status status) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= repository.getSize())
            throw new IndexIncorrectException();
        if (status == null)
            throw new StatusEmptyException();
        @NotNull final Task task = findOneByIndex(userId, index);
        if (task == null)
            throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusId(@NotNull final String userId,
                                   @NotNull final String id,
                                   @NotNull final Status status) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (id == null || id.isEmpty())
            throw new TaskIdEmptyException();
        if (status == null)
            throw new StatusEmptyException();
        @NotNull final Task task = findOneById(userId, id);
        if (task == null)
            throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(userId, projectId);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        return repository.getSize();
    }
    
}

