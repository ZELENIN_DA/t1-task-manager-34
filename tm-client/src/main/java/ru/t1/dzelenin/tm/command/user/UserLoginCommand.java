package ru.t1.dzelenin.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.request.user.UserLoginRequest;
import ru.t1.dzelenin.tm.dto.response.user.UserLoginResponse;
import ru.t1.dzelenin.tm.enumerated.Role;
import ru.t1.dzelenin.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "login";

    @NotNull
    private final String DESCRIPTION = "user login.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserLoginRequest request = new UserLoginRequest(getToken(), login, password);
        @NotNull final UserLoginResponse response = getServiceLocator().getAuthEndpoint().login(request);
        setToken(response.getToken());
        System.out.println(response.getToken());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}

